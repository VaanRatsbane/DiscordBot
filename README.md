# DiscordBot
Discord guild administration/service bot. Made using [DSharpPlus](https://github.com/DSharpPlus/DSharpPlus)

This bot is highly specific to my guild and thus will not work under one with a different channel/role layout, or where the bot does not have full permissions.

It is not meant to be run in multiple guilds at once due to its high specificity.

It also expects the existance of a number of settings/tokens to be set during runtime before certain commands are evoked. This is a one time process.

As such this repo's existance is to facilitate working in different environments, and streamline the updating of the bot in my VPS.

With all that said, feel free to use any part of my code for whatever purpose you wish.
