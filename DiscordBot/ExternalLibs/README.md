﻿# References
This is a list of external references that are not in nuget, and thus won't be rehosted in the repository. They are essential for some features of the bot.

* [source-query-net](https://github.com/brycekahle/source-query-net) by [brycekahle](https://github.com/brycekahle)