﻿using System.Collections.Generic;
using System.Reflection;
using DSharpPlus.CommandsNext;

namespace DiscordBot
{

    public static class CommandsNextExtensions
    {

        /// <summary>
        /// Unregisters a module from the available commands list. Courtesy of _moonPtr#8058 @disc
        /// </summary>
        /// <typeparam name="T">The module to unload</typeparam>
        /// <param name="cnext">The commandsnextmodule which will be extended with this method</param>
        public static void UnregisterCommands<T>(this CommandsNextModule cnext) where T : class
        {
            // Command Dictionary Property
            var cdict = cnext.GetType().GetProperty("TopLevelCommands", BindingFlags.NonPublic | BindingFlags.Instance);
            // Original Dictionary
            var odict = cdict.GetValue(cnext) as Dictionary<string,Command>;
            // Temporary Dictionary
            var tdict = new Dictionary<string, Command>();
            // Register commands into empty dictionary
            cdict.SetValue(cnext, tdict);
            cnext.RegisterCommands<T>();
            // Iterate over temporary dictionary and remove commands from the original dictionary
            foreach (var x in tdict)
                odict.Remove(x.Key);
            // Put original dictionary back
            cdict.SetValue(cnext, odict);
        }
    }
}