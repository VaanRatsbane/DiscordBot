﻿using DSharpPlus.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Timers;
using System.Xml;

namespace DiscordBot.Modules.API
{
    public class NexusMods : IKillable
    {
        const string NEXUSFEED_PATH = "Files/API/nexusfeed.json";
        const string NEXUS_LOGO = "https://vignette.wikia.nocookie.net/tes-mods/images/e/e8/NMM_Icon.png";
        const string NEXUS_NEW = "https://www.nexusmods.com/@GAME@/rss/newtoday";
        const string NEXUS_UPDATED = "https://www.nexusmods.com/@GAME@/rss/updatedtoday";
        const string NEXUS_NEW_LINK = "https://www.nexusmods.com/@GAME@/search/?searchtype=mods&subtype=1&BH=1";
        const string NEXUS_LASTUPDATED_LINK = "https://www.nexusmods.com/@GAME@/search/?searchtype=mods&subtype=1&BH=0";

        private List<Feed> feeds;
        private Timer feedTimer;
        private List<ReadMod> readMods;

        internal class ReadMod
        {
            public DateTime timeRead;
            public string guid;
        }

        public NexusMods()
        {
            if (!Directory.Exists("Files/API"))
                Directory.CreateDirectory("Files/API");

            try
            {
                var json = File.ReadAllText(NEXUSFEED_PATH);
                var feedData = JsonConvert.DeserializeObject<FeedSave>(json);

                feeds = feedData.feeds;
                readMods = feedData.ReadMods;

                Log.Success("Loaded nexus feeds.");
            }
            catch
            {
                Log.Warning("Couldn't load nexus feeds. Initializing...");
                feeds = new List<Feed>();
                readMods = new List<ReadMod>();
            }

            feedTimer = new Timer();
            feedTimer.AutoReset = true;
            feedTimer.Elapsed += FeedTimer_Elapsed;
            feedTimer.Interval = 1000 * 60 * 10; //six times an hour
            if (feeds.Count > 0)
            {
                feedTimer.Start();
            }
        }

        public bool Add(ulong channelId, string game)
        {
            if (!feeds.Any(c => c.channelId == channelId))
            {
                feeds.Add(new Feed() { channelId = channelId, feed = game });
                Save();
                if (feeds.Count == 1)
                {
                    feedTimer.Start();
                }
                return true;
            }
            else
                return false;
        }

        public bool Remove(string game)
        {
            if (feeds.Any(c => c.feed == game))
            {
                feeds.Remove(feeds.First(c => c.feed == game));
                Save();
                if (feeds.Count == 0)
                    feedTimer.Stop();
                return true;
            }
            else
                return false;
        }

        public void Save()
        {
            try
            {
                var data = new FeedSave()
                {
                    feeds = feeds,
                    ReadMods = readMods
                };
                var json = JsonConvert.SerializeObject(data, Newtonsoft.Json.Formatting.Indented);
                File.WriteAllText(NEXUSFEED_PATH, json);
            }
            catch (Exception e)
            {
                Log.Error("Failed to save nexus feed data! Error:\n" + e.ToString());
            }
        }

        public void Kill()
        {
            feedTimer.Stop();
        }

        private async void FeedTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var guild = await Program._discord.GetGuildAsync(ulong.Parse(Program.cfg.GetValue("guild")));

            foreach (var feed in feeds)
            {
                var channel = guild.GetChannel(feed.channelId);
                if(!(channel is null))
                {
                    try
                    {
                        var now = DateTime.UtcNow;

                        var toKeep = new List<ReadMod>();
                        foreach (var mod in readMods)
                        {
                            if (now - mod.timeRead < TimeSpan.FromDays(1))
                                toKeep.Add(mod);
                        }
                        readMods = toKeep;

                        {
                            var newRSS = NEXUS_NEW.Replace("@GAME@", feed.feed);
                            XmlDocument xml = new XmlDocument();
                            xml.Load(newRSS);
                            var root = xml.DocumentElement;
                            var nodes = root.SelectNodes("/rss/channel/item");
                            foreach (XmlNode node in nodes)
                            {
                                var guid = node["guid"].InnerText;

                                if (readMods.Any(c => c.guid == guid)) continue; //in case there's duplicates

                                var title = node["title"].InnerText;
                                var category = node["category"].InnerText;
                                var summary = node["nexusmods:summary"].InnerText;
                                var author = node["author"].InnerText;
                                var link = node["link"].InnerText;
                                var image = node["enclosure"].GetAttribute("url");

                                var embed = new DiscordEmbedBuilder()
                                    .WithAuthor($"New Nexus Mod", NEXUS_NEW_LINK.Replace("@GAME@", feed.feed), NEXUS_LOGO)
                                    .WithTitle($"[{title}]({link})")
                                    .AddField("Created by", author)
                                    .AddField("Category", category)
                                    .AddField("About", summary)
                                    .WithImageUrl(image)
                                    .WithColor(DiscordColor.Orange);

                                await channel.SendMessageAsync(embed: embed);

                                readMods.Add(new ReadMod()
                                {
                                    guid = guid,
                                    timeRead = now
                                });
                            }
                        }
                        {
                            var updatedRSS = NEXUS_UPDATED.Replace("@GAME@", feed.feed);
                            XmlDocument xml = new XmlDocument();
                            xml.Load(updatedRSS);
                            var root = xml.DocumentElement;
                            var nodes = root.SelectNodes("/rss/channel/item");
                            foreach (XmlNode node in nodes)
                            {
                                var guid = node["guid"].InnerText;

                                if (readMods.Any(c => c.guid == guid)) continue; //in case there's duplicates, mainly avoids reposting new mods

                                var title = node["title"].InnerText;
                                var category = node["category"].InnerText;
                                var summary = node["nexusmods:summary"].InnerText;
                                var author = node["author"].InnerText;
                                var link = node["link"].InnerText;
                                var image = node["enclosure"].GetAttribute("url");

                                var embed = new DiscordEmbedBuilder()
                                    .WithAuthor($"Updated Nexus Mod", NEXUS_LASTUPDATED_LINK.Replace("@GAME@", feed.feed), NEXUS_LOGO)
                                    .WithTitle($"[{title}]({link})")
                                    .AddField("Created by", author)
                                    .AddField("Category", category)
                                    .AddField("About", summary)
                                    .WithThumbnailUrl(image)
                                    .WithColor(DiscordColor.Orange);

                                var changelog = node["nexusmods:changelog"];
                                if (!(changelog is null) && changelog.ChildNodes.Count > 0)
                                    embed = new DiscordEmbedBuilder(embed)
                                        .AddField($"Version: {changelog.ChildNodes[0]["nexusmods:version"].InnerText.Trim()}", changelog.ChildNodes[0]["nexusmods:change"].InnerText.Trim());

                                await channel.SendMessageAsync(embed: embed);

                                readMods.Add(new ReadMod()
                                {
                                    guid = guid,
                                    timeRead = now
                                });
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        Log.Warning("Nexus feed:\n" + ex.ToString());
                    }
                }
            }
        }

        internal class FeedSave
        {
            public List<ReadMod> ReadMods;
            public List<Feed> feeds;
        }

        internal class Feed
        {
            public ulong channelId;
            public string feed;
        }
    }
}
